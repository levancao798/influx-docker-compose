# swarmstack/influxdb

## DEPLOY INFLUXDB AS A STACK

```
INFLUXDB_ADMIN_USER='admin' \
INFLUXDB_ADMIN_PASSWORD='admin' \
INFLUXDB_USER='prometheus' \
INFLUXDB_USER_PASSWORD='prompass' \
docker stack deploy -c docker-compose.yml influxdb
```

Chỉnh sửa lại các biến trong file compose, sau đó chạy lệnh sau để triển khai
```docker stack deploy -c docker-compose.yml influxdb```

## Thiết lập influxdb
Connected to http://localhost:8086 <br>


Tạo InfluxDB Database <br>
SQL command: CREATE DATABASE sonarqube_kpi <br>
Web: Đăng nhập > Chọn Bucket > Chọn new bucket> Nhập tên bucket cần tạo > Bấm create 


## Grafana Datasource 

Từ grafana setting -> Datasource -> Add datasource <br>
Chọn kiểu database là influxdb


